package ru.kuzin.tm.exception.user;

import ru.kuzin.tm.exception.field.AbstractFieldException;

public final class AccessDeniedException extends AbstractFieldException {

    public AccessDeniedException() {
        super("Error! You are not logged in. Please try again...");
    }

}